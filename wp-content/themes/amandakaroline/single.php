<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Amanda_Karoline
 */
global $configuracao;
get_header();
?>

		<!-- PÁGINA DE POST -->
		<div class="pg pg-post" style="display:" >
			<!-- DIV CONTAINER - BOOTSTRAP -->
			<div class="container">
			<?php
			
				$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoBlog = $fotoBlog[0];
				global $post;
				$categories = get_the_category();
				
				?>
				<!-- DIV DE TITULO DO POST -->
				 <div class="tituloPost">
					<?php
						$categories =get_the_category();

						foreach ($categories as $categories):
							$nomeCategoria = $categories->name;
						
							if($nomeCategoria  != "destaqueLeft" && $nomeCategoria  != "destaqueRight" ):
						
					?>
					<h3><?php echo $nomeCategoria ?> <!-- importante transformar o testo para uppercase --></h3>
					<?php endif; endforeach;  ?>
					
					<h1><?php echo the_title() ?></h1>
					<span><?php the_time('j/m/Y')?></span>
				</div>

				<figure style="background:url(<?php echo $fotoBlog ?>)" alt="<?php echo get_the_title() ?>)">
					<img src="<?php echo $fotoBlog ?>" alt="<?php echo get_the_title() ?>">
				</figure>

				<!-- DIV DE CONTEUDO DO POST -->
				<article class="textoPost">
					<p><?php echo the_content(); ?></p>
				</article>

				<!-- DIV DE DESCRIÇÃO DO AUTOR -->
				<div class="autor">
				    <?php $user = wp_get_current_user(); if($user): ?>
						<?php echo get_avatar(get_the_author_meta('ID'), 96); // IMAGEM DO AUTOR ?> 
						<h2><?php echo get_the_author(); //NOME DO AUTOR ?></h2>
						<span>Autor</span>
					<?php endif; ?>
					<div class="redesSociais">
						<?php if($configuracao['opt_whatsapp']): ?>	
							<a href="<?php echo $configuracao['opt_whatsapp'] ?>" target="_blank">
								<i class="fab fa-whatsapp"></i>
							</a>
						<?php endif;

			 				if($configuracao['opt_instagram']):
						?>
							<a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank">
								<i class="fab fa-instagram"></i>
							</a>
						<?php endif;

			 				if($configuracao['opt_pinterest']):
						?>
							<a href="<?php echo $configuracao['opt_pinterest'] ?>" target="_blank">
								<i class="fab fa-pinterest-p"></i>
							</a>
						<?php endif;

			 				if($configuracao['opt_facebook']):
						?>
							<a href="<?php echo $configuracao['opt_facebook'] ?>" target="_blank">
								<i class="fab fa-facebook-f"></i>
							</a>
						<?php endif; ?>
					</div>
				</div>
				
				<!-- DISQUS -->
				<div class="disqus">
					<?php 
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
						    comments_template();
						endif; 
					?>
				</div>
					
				<div class="leiaMais">
					<span>Leia mais:</span>
				</div>	

				<div class="categorias">
					
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<!-- NAV DE CATEGORIAS -->
						<?php 
							$todasAsCategorias = get_categories();
							foreach ($todasAsCategorias as $todasAsCategorias) : 

							 	if($todasAsCategorias->name != "destaqueLeft" && $todasAsCategorias->name != "destaqueRight"):
						 			$nomeDaCategoria = $todasAsCategorias->name;
						 			$id_categoria = $todasAsCategorias->term_id;
		 	 			?>	
						<li class="nav-item">
							<a class="nav-link active nomeCategorias" id="home-tab" data-toggle="tab" href="#<?php echo $id_categoria; ?>" role="tab" aria-controls="home" aria-selected="true"><?php echo $nomeDaCategoria; ?></a>
						</li>
						<?php endif; endforeach; ?>
					</ul>

					<div class="tab-content" id="myTabContent">
						<?php 
							// CRIACAO DO CONTADOR
							$contador = 2;
							// A VARIAVEL "todasAsCategorias " VAI RECEBER AS CATEGORIAS PRESENTES NO WP
							$todasAsCategorias = get_categories();
							// LOOP DE CATEGORIAS
							foreach ($todasAsCategorias as $todasAsCategorias) : 
								// VERIFICACAO SE O NOME DAS CATEGORIAS É DIFERENTE DAS CATEGORIAS PRESENTES 
							 	if($todasAsCategorias->name != "destaqueLeft" && $todasAsCategorias->name != "destaqueRight"):
						 			
						 			$id_categoria = $todasAsCategorias->term_id;
	 	 					if($contador == 2): 
		 	 			?>
								<div class="tab-pane fade active in" id="<?php echo $id_categoria; ?>" role="tabpanel" aria-labelledby="home-tab">
									<div class="posts active">
											<?php 
												//FILTRO CATEGORIA POST			
												$filtroDePostPorCategoria = new WP_Query(array(
													// TIPO DE POST
													'post_type'     => 'post',
													// POST POR PAGINA
													'posts_per_page'   => 3,
													// FILTRANDO PELA CATEGORIA DESTAQUE
													'tax_query'     => array(
														array(
															// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
															'taxonomy' => 'category',
															// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
															'field'    => 'term_id',
															// SLUG DA CATEGORIA
															'terms'    => $todasAsCategorias->term_id,
															)
														)
													)
												);	
											
											?>
										
		                                <ul>
		                                	<?php 
		                                	// LOOP DE POST
												while ( $filtroDePostPorCategoria->have_posts() ) : $filtroDePostPorCategoria->the_post();
													$fotoDestaquePostCategoria = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
													$fotoDestaquePostCategoria = $fotoDestaquePostCategoria[0];
										 	?>		
		                                    <li>
		                                        <a href="<?php echo get_permalink(); ?>">
		                                            <figure class="imagemCarrossel"  style="background: url(<?php echo $fotoDestaquePostCategoria ?>);">
		                                                <img src="<?php echo $fotoDestaquePostCategoria ?>" alt="<?php echo get_the_title() ?>">
		                                                <span class="leiaMais">Leia Mais</span>
		                                            </figure>
		                                            <article>
		                                                <span><?php echo $todasAsCategorias->name; ?> - <?php the_time('j/F/Y');?></span>
		                                                <h2><?php echo get_the_title(); ?></h2>
		                                                <p><?php customExcerpt(90); ?></p>
		                                            </article>
		                                        </a>
		                                    </li>
		                                   <?php endwhile; ?>
		                                </ul>
									</div>
								</div>
							<?php else: ?>
								<div class="tab-pane fade" id="<?php echo $id_categoria; ?>" role="tabpanel" aria-labelledby="home-tab">
									<div class="posts active">
											<?php 
											//FILTRO CATEGORIA POST			
											$filtroDePostPorCategoria = new WP_Query(array(
												// TIPO DE POST
												'post_type'     => 'post',
												// POST POR PAGINA
												'posts_per_page'   => -1,
												// FILTRANDO PELA CATEGORIA DESTAQUE
												'tax_query'     => array(
													array(
														// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
														'taxonomy' => 'category',
														// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
														'field'    => 'term_id',
														// SLUG DA CATEGORIA
														'terms'    => $todasAsCategorias->term_id,
														)
													)
												)
											);	
										
										?>
										
		                                <ul>
		                                	<?php 
		                                	// LOOP DE POST
												while ( $filtroDePostPorCategoria->have_posts() ) : $filtroDePostPorCategoria->the_post();
													$fotoDestaquePostCategoria = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
													$fotoDestaquePostCategoria = $fotoDestaquePostCategoria[0];
										 	?>		
		                                    <li>
		                                        <a href="<?php echo get_permalink(); ?>">
		                                            <figure class="imagemCarrossel"  style="background: url(<?php echo $fotoDestaquePostCategoria ?>);">
		                                                <img src="<?php echo $fotoDestaquePostCategoria ?>" alt="<?php echo get_the_title() ?>">
		                                                <span class="leiaMais">Leia Mais</span>
		                                            </figure>
		                                            <article>
		                                                <span><?php echo $todasAsCategorias->name; ?> - <?php the_time('j/F/Y');?></span>
		                                                <h2><?php echo get_the_title(); ?></h2>
		                                                <p><?php customExcerpt(90); ?></p>
		                                            </article>
		                                        </a>
		                                    </li>
		                                   <?php endwhile; ?>
		                                </ul>
									</div>
								</div>
							<?php 
							endif; 
								endif; 
								$contador++;
						endforeach; ?>
					</div>
					
				</div>
			</div>
			<div class="compartilhar">
				<div class="balao-compartilhar">
					<small></small>	
				</div>
						
				<div id="mostre" class="links-compartilhamento">
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_url; ?>&t=TITLE" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fab fa-facebook-f"></i>
					</a>
					<a href="https://plus.google.com/share?url=<?php echo $current_url; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fab fa-google-plus-g"></i>
					</a>
					<a href="http://twitter.com/share?text=Confira%20esta%20notícia:&hashtags=post_compartilhado,artigo_interessante" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
						<i class="fab fa-twitter"></i>	
					</a>
				</div>
				<span  id="click">
					<i class="fas fa-share-alt"></i>
				</span>		
			</div>
		</div>


	

		

		

<?php
get_footer();
