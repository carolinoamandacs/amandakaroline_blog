<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amanda_Karoline



 */
global $configuracao;
get_header();
?>
<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		<!-- REDES SOCIAIS FIXAS NA PAGINA INICIAL -->
		<div class="redesFixas">
				<div class="fixas">
					<?php if($configuracao['opt_whatsapp']): ?>
					<a href="<?php echo $configuracao['opt_whatsapp']?>" target="_blank"	>
						<i class="fab fa-whatsapp"></i>
					</a>
					<?php endif;

			 			if($configuracao['opt_instagram']):
					?>
					<a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank">
						<i class="fab fa-instagram"></i>
					</a>
					<?php endif;

			 			if($configuracao['opt_pinterest']):
					?>
					<a href="<?php echo $configuracao['opt_pinterest'] ?>" target="_blank">
						<i class="fab fa-pinterest-p"></i>
					</a>
					<?php endif;

			 			if($configuracao['opt_facebook']):
					?>
					<a href="<?php echo $configuracao['opt_facebook'] ?>" target="_blank">
						<i class="fab fa-facebook-f"></i>
					</a>
					<?php endif; ?>
				</div>
		</div>


		<section class="carrosselDestaque">
			<h6 class="hidden">Carrossel de Destaque</h6>
			
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<?php 
							//LOOP DE POST CATEGORIA DESTAQUE				
							$destaqueLeft = new WP_Query(array(
								// TIPO DE POST
								'post_type'     => 'post',
								// POST POR PAGINA
								'posts_per_page'   => -1,
								// FILTRANDO PELA CATEGORIA DESTAQUE
								'tax_query'     => array(
									array(
										// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
										'taxonomy' => 'category',
										// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
										'field'    => 'slug',
										// SLUG DA CATEGORIA
										'terms'    => 'destaque-left',
										)
									)
								)
							);
							if ($destaqueLeft):
						?>

						<div id="carrosselDestaqueLeft" class="owl-Carousel">
							<?php 
				
								// LOOP DE POST
								while ( $destaqueLeft->have_posts() ) : $destaqueLeft->the_post();
									//FOTO DESTACADA
									$fotoDestaqueLeft = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$fotoDestaqueLeft = $fotoDestaqueLeft[0];
									
							?>		
								<div class="item">
									<a href="<?php echo get_permalink(); ?>">
									<figure style="background: url(<?php echo $fotoDestaqueLeft ?>)"></figure>
								
										<h2><?php echo get_the_title(); ?></h2>
									</a>
								</div>
							<?php endwhile; endif; wp_reset_query();?>
						</div>
					</div>
					<div class="col-sm-6">
						<?php 
							//LOOP DE POST CATEGORIA DESTAQUE				
							$destaqueRight = new WP_Query(array(
								// TIPO DE POST
								'post_type'     => 'post',
								// POST POR PAGINA
								'posts_per_page'   => -1,
								// FILTRANDO PELA CATEGORIA DESTAQUE
								'tax_query'     => array(
									array(
										// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
										'taxonomy' => 'category',
										// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
										'field'    => 'slug',
										// SLUG DA CATEGORIA
										'terms'    => 'destaque-right',
										)
									)
								)
							);
							if ($destaqueRight):
						?>
						<div id="carrosselDestaqueRight" class="owl-Carousel">
							<?php 
				
								// LOOP DE POST
								while ( $destaqueRight->have_posts() ) : $destaqueRight->the_post();
									//FOTO DESTACADA
									$fotoDestaqueRight = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$fotoDestaqueRight = $fotoDestaqueRight[0];
									
							?>		
								<div class="item">
									<a href="<?php echo get_permalink(); ?>">
									<figure style="background: url(<?php echo $fotoDestaqueRight ?>)"></figure>
								
										<h2><?php echo get_the_title(); ?></h2>
									</a>
								</div>
							<?php endwhile; endif; wp_reset_query();?>
						</div>
					</div>
				</div>
			</div>
			
		</section>

		<section class="carrosselCategorias">
			<h6 class="hidden">Carrossel de Categoria</h6>
			<div class="container">
				
				<div id="carrosselCategoria" class="owl-Carousel">
					<!-- LOOP DE CATEGORIAS --> 
					<?php 
						$categorias = get_categories();
						 foreach ($categorias as $categorias):
								$categoria = $categorias;
								$imagemCategoria = z_taxonomy_image_url($categoria->cat_ID);
						 	if($categoria->slug != "destaque-left" && $categoria->slug != "destaque-right"): 

					?>
					<figure class="item">
						<!-- LINK DA CATEGORIA -->
						<a href="<?php echo get_category_link($categoria->cat_ID); ?>">
							<?php if($imagemCategoria): ?>
								<!-- IMAGEM DA CATEGORIA -->
								<img src="<?php echo $imagemCategoria; ?>" alt="<?php echo $categoria->name;?>">
							<?php endif; ?>
							<!-- NOME DA CATEGORIA --> 
							<h2><?php echo $categoria->name; ?></h2>
						</a>
					</figure>
					<!-- FIM DO LOOP DE CATEGORIAS -->
					<?php endif; endforeach; wp_reset_query();?>
				</div>
			</div>

		</section>

		<nav class="menuCategorias" style="display: none;">
			<?php 
				$categorias = get_categories();
				 foreach ($categorias as $categorias):
					$categoria = $categorias;
					//$imagemCategoria = z_taxonomy_image_url($categoria->cat_ID);
					if($categoria->slug != "destaque-left" && $categoria->slug != "destaque-right"): 
			?>	
				<a href="<?php echo get_category_link($categoria->cat_ID); ?>">
					<figure style="background:url(<?php echo $categoria->description ?>);">
						<img src="<?php echo $categoria->description ?>" alt="<?php echo $categoria->name; ?>">
					</figure>
					<?php echo $categoria->name; ?>		
				</a>
			<?php endif;endforeach; ?>
		</nav>

		<section class="areaPost" style="display: ;">
			<h6 class="hidden">Área de posts</h6>
			
			<div class="container">
				<?php echo do_shortcode('[ajax_load_more post_type="post" repeater="default" posts_per_page="2" transition="fade" button_label="Ver Mais"]' ) ?>

			</div>
		</section>
		
		<section class="areaProdutos" style="display: none;">
			<div class="container">
				<h6 class="tituloSessao" >Conheça nossa loja:</h6>
			</div>
		</section>

		<div class="feedInstagram">

			<a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank" class="linkInstagram"><i class="fab fa-instagram"></i> @amandakarolineofc</a>
			<?php echo do_shortcode('[instagram-feed num=3 cols=3 width=100 imagepadding=1 disablemobile=true]'); ?>
		</div>
	</div>

		
		

<?php
get_footer();
