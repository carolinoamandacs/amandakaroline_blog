<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amanda_Karoline
 */
global $configuracao;
?>	
	<!-- BOTÃO VOLTAR AO TOPO -->
	<div class="backToTop" style="display: none;">
		<span id="subir">
			<i class="fas fa-chevron-up"></i>
		</span>
	</div>


	<!-- RODAPÉ -->
<footer class="rodape">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<a href="<?php echo get_home_url() ?>" class="linkLogoRodape">
					<figure>
						<img src="<?php echo $configuracao['opt_logo_rodape']['url'] ?>" alt="Imagem">
					</figure>
				</a>
				
			</div>
			<div class="col-sm-6">
				<div class="copyright"><p><?php echo $configuracao['opt_copyright'] ?></p></div>
			</div>
			<div class="col-sm-3">
				<div class="redesSociaisRodape">
					<ul class="listaRedesSociaisRodape">
						<?php 
							if($configuracao['opt_instagram']):
					 	?>
						<li><a href="<?php echo $configuracao['opt_instagram'] ?>" class="instagram" target="_blank"><img src="<?php echo $configuracao['opt_instagram_rodape']['url'] ?>" alt="img"></a></li>
						<?php 
							endif;
							if($configuracao['opt_facebook']):
						?>
						<li><a href="<?php echo $configuracao['opt_facebook'] ?>" class="facebook" target="_blank"><img src="<?php echo $configuracao['opt_facebook_rodape']['url'] ?>" alt="img"></a></li>
						<?php 
							endif;
							if($configuracao['opt_pinterest']):
						?>
						<li><a href="<?php echo $configuracao['opt_pinterest'] ?>" class="pinterest" target="_blank"><img src="<?php echo $configuracao['opt_pinterest_rodape']['url'] ?>" alt="img"></a></li>
						<?php 
							endif;
							if($configuracao['opt_link_loja']):
						?>
						<li><a href="<?php echo $configuracao['opt_link_loja'] ?>" title="Acesse a minha loja!" class="shoppingCart" target="_blank"><img src="<?php echo $configuracao['opt_imagem_loja']['url'] ?>" alt="img"></a></li>
						<?php 
							endif;
							if($configuracao['opt_link_portfolio']):
						?>
						<li><a href="<?php echo $configuracao['opt_link_portfolio'] ?>" title="Acesse o meu portfólio!" class="folder" target="_blank"><img src="<?php echo $configuracao['opt_imagem_portfolio']['url'] ?>" alt="img"></a></li>
						<?php 
							endif;
						?>
						<li><a href="<?php echo get_home_url(); ?>" title="Acesse o meu blog!" class="editor" target="_blank"><img src="<?php echo $configuracao['opt_blog_rodape']['url'] ?>" alt="img"></a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</footer>
<?php wp_footer(); ?>

</body>
</html>
