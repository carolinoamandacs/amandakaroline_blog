<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Amanda_Karoline
 */

get_header();
?>
					
<div class="pg pg-categorias">
	<div class="containerLargura">
		<div class="resultadoPesquisa">
			<?php
				/* translators: %s: search query. */
				printf( esc_html__( 'Resultado da busca por: %s', 'amandakaroline' ), '<span>' . get_search_query() . '</span>' );
			?>
		</div>
		<!-- NAVS POSTAGENS MAIS LIDAS/RECENTES -->
		<nav>
			<!-- NAV DE CATEGORIAS -->
			<?php 
				$todasAsCategorias = get_categories();
				foreach ($todasAsCategorias as $todasAsCategorias) : 

				 	if($todasAsCategorias->name != "destaqueLeft" && $todasAsCategorias->name != "destaqueRight"):
			 			$nomeDaCategoria = $todasAsCategorias->name;
			 			$id_categoria = $todasAsCategorias->term_id;
			 		
	 			?>	
				<a href="<?php echo get_category_link($todasAsCategorias->cat_ID); ?>" >
					<?php echo $nomeDaCategoria ?>
				</a>
			<?php endif; endforeach; ?>
		</nav>


			<!-- LISTA DE POSTS DA CATEGORIA -->
			<section class="categoriaDePost">
				<ul>
				 <?php 
				 	if (have_posts()): 
				 	
						while (have_posts()) : 
							the_post();
							
					 		$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoPost = $fotoPost[0]; 
							
							get_queried_object();

							$category= get_queried_object();
				?>
					<li>
						<a href="<?php echo get_permalink(); ?>">
							<figure style="background: url(<?php echo $fotoPost; ?>);">
								<img src="<?php echo $fotoPost; ?>" alt="imagem-post">
								<span class="leiaMais">Leia Mais</span>
							</figure>
							<article>
								<?php 
									$todasAsCategorias = get_categories();
									foreach ($todasAsCategorias as $todasAsCategorias) :
										if($todasAsCategorias->name != "destaqueLeft" && $todasAsCategorias->name != "destaqueRight"):
								 			$nomeDaCategoria = $todasAsCategorias->name;
			 					?>
								<span><?php echo $nomeDaCategoria." - "; endif; endforeach;?><?php the_time('j/m/Y'); ?></span>
								<h2><?php echo get_the_title(); ?></h2>
								<p><?php customExcerpt(150); ?></p>
							</article>
						</a>
					</li>
				<?php endwhile; else: ?>

					<h1 id="resultadoNaoEncontrado">Não encontramos sua busca por: <?php echo get_search_query()." !" ?> </h1><a href="<?php echo home_url('/'); ?>" id="volteParaAPaginaInicial">Continue navegando</a>
				<?php endif; ?>


							<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
			
			</section>
	</div>
</div>
	

<?php
get_footer();
