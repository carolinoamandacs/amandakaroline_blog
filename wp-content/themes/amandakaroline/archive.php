<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
		
	
 *
 * @package Amanda_Karoline
 */

get_header();
?>
<!-- PÁGINA DE CATEGORIAS -->
	<div class="pg pg-categorias"  >
		<div class="containerLargura">
			<!-- IMAGEM DE BANNER DE CATEGORIA -->
			<figure class="banner">
				<?php 
					$bannerCategoria = get_queried_object();
					$categoriaAtual = get_queried_object()->term_id;
				?>
					
				<img src="<?php echo $bannerCategoria->description ?>" alt="">
			</figure>
			<!-- NAVS POSTAGENS MAIS LIDAS/RECENTES -->
			<nav>
				<!-- NAV DE CATEGORIAS -->
				<?php 
					$todasAsCategorias = get_categories();
					foreach ($todasAsCategorias as $todasAsCategorias) : 

					 	if($todasAsCategorias->name != "destaqueLeft" && $todasAsCategorias->name != "destaqueRight"):
				 			$nomeDaCategoria = $todasAsCategorias->name;
				 			$id_categoria = $todasAsCategorias->term_id;
				 		if($categoriaAtual == $id_categoria){
				 			$classe = "ativo";
				 		}
				 		else {
				 			$classe = " ";
				 		}
 	 			?>	
					<a href="<?php echo get_category_link($todasAsCategorias->cat_ID); ?>" class="<?php echo $classe; ?>">
						<?php echo $nomeDaCategoria ?>
					</a>
				<?php endif; endforeach; ?>
			</nav>


				<!-- LISTA DE POSTS DA CATEGORIA -->
				<section class="categoriaDePost">
					<ul>
					 <?php 
						while (have_posts()) : 
							the_post();
							
					 		$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoPost = $fotoPost[0]; 
							
							get_queried_object();

							$category= get_queried_object();
					?>
						<li>
							<a href="<?php echo get_permalink(); ?>">
								<figure style="background: url(<?php echo $fotoPost; ?>);">
									<img src="<?php echo $fotoPost; ?>" alt="imagem-post">
									<span class="leiaMais">Leia Mais</span>
								</figure>
								<article>
									<span><?php echo $category->name; ?>-<?php the_time('j/m/Y'); ?></span>
									<h2><?php echo get_the_title(); ?></h2>
									<p><?php customExcerpt(150); ?></p>
								</article>
							</a>
						</li>
					<?php endwhile; ?>
								<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
				
				</section>
		</div>
	</div>
	
<?php
get_footer();
