<?php $fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoBlog = $fotoBlog[0]; 
get_queried_object();

$category= get_queried_object();

?>
<li class="">
	<a href="<?php the_permalink(); ?>" class="linkPostFoto">
		<figure class="hvr-grow" style="background:url(<?php echo $fotoBlog ?>)" alt="<?php echo get_the_title() ?>)"></figure>
	</a>
	<article>
		<?php 
			$todasAsCategorias = get_categories();
			$categoriaAtual = $todasAsCategorias[0]->cat_name;
			
				if($categoriaAtual->name != "destaqueLeft" && $categoriaAtual->name != "destaqueRight"):
					$nomeDaCategoria = $categoriaAtual;
		 			
			 ?>
		<h3><?php echo $nomeDaCategoria." -";?></h3>
		<?php endif; ?>
		<span><?php the_time('j/m/Y') ?></span>
		<h2><?php echo get_the_title() ?></h2>
		<p><?php customExcerpt(150); ?></p>
	</article>
	<div class="row">
		<div class="col-xs-4">
			<div class="redesSociais">
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink();  ?>&t=TITLE" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					<i class="fab fa-facebook-square"></i>
				</a>
        			
			</div>
		</div>
		<div class="col-xs-4">
			<a href="<?php the_permalink() ?>" class="linkPost">
				<span class="lerMais">Ler mais</span>
			</a>
		</div>
		<div class="col-xs-4">
			<div class="redesSociais">
				<a href="http://twitter.com/share?text=Confira%20esta%20notícia:&hashtags=post_compartilhado,artigo_interessante" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					<i class="fab fa-twitter-square"></i>
				</a>
				
			</div>
		</div>
	</div>
							
</li>