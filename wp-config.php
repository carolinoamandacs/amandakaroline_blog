<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/home1/amandakaroline/public_html/blog/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'amandaka_blog');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'amandaka_root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'a@hc1468');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@M%2uN|Sy<n;I>u0M41tZ?Bz$/yO;*OzoDcz*=H]-bTk{&D-ZQ$0:x.0p+RQ:-gV');
define('SECURE_AUTH_KEY',  '([ErnowXtZlTfuu7.XK}xoad7FUB]kzB;26nsJMw]n;[P5%)DRG{+:OnHee4Asb)');
define('LOGGED_IN_KEY',    '!`1,BWpDr/nJV.F-h+u;VrgL0z,|TtER;HYhUx7bVqk8bl`DsV0,)~So1Y(ULvw ');
define('NONCE_KEY',        'rz~faLgB-xsW,K0sKUR*uaZ->t%fDQex-IbN-/WVoG6/x:v:=Pk/@4-`iA 2cbQH');
define('AUTH_SALT',        'wTUcf/56m*1wh$QZ./J<)<$n%P!j)p)N{jR<GiD~m=$@sO1A,B2^>3dP_|_aJ ]r');
define('SECURE_AUTH_SALT', 'ndb&!(IHb-/$<D=9S7<i.0fl,9VX$5<n*GWJaDmVS+o+Y~C1bM>:1-$t_tF_7#?+');
define('LOGGED_IN_SALT',   '6xbe$b^Zy4mL8![vtc`{^sF2enC*kx!4qFAR9Mc1+L@8!i]r 9kzZi0K3~S6mDac');
define('NONCE_SALT',       'p5%=&$;}ee+L}FtrnRan0DBdL(o!YFSq81|%e|Sk<m`-)=*9^Xa[oeG?O_aD( ;V');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'ak_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);